import pycurl, os, json, zlib, random,sys, urllib.parse, time, gzip,io , zlib,base64
from io import StringIO
from io import BytesIO
class cPanel:

    def request(self, *argv,**kwargs):
        try:
            if self.cFile:
                try:
                    open(self.cFile, "w")
                except:
                    if self.cFile:
                        print("[WARN] CHECK COOKIE: "+self.cFile)
            elif self.cFile:
                print("[WARN] NO R/W PERMMISION ON COOKIE: \n"+self.cFile)

            ### THIS FOR ALLOW GET THE INFO FROM THE CONNECTION
            
            ret = BytesIO()
            self.ret = BytesIO()
            p = pycurl.Curl()

            ### THIS ARE THE OPTIONS SET FOR THE PYCURL,
            #   FEEL FREE TO TWEAK THESE IF YOU KNOW WHAT 'R YOU DOING
            #   BUT LET ME WARN YOU THAT YOU MAY FACE TROUBLES IF YOU DON'T
            #   UNDERSTAND WHAT ARE THEY FOR  
            #   IF YOU HAVE AN SLOW CONNECTION, YOU COULD INCREASE TIMEOUT VALUES
            p.setopt(pycurl.CONNECTTIMEOUT, 10)
            p.setopt(pycurl.TIMEOUT, 10)

            ### AND AS IS KINDA TRICKY KEEP TRACK OF THE SESSION COOKIE IN PYTHON
            #   I DID A BIT OF RESEARCH AND ENDED IN USING AUTHENTICATION IN THE HEADERS
            #   ACTUALLY IS RISKY BUT IN THE MEANWHILE THIS IS THE BEST SHORT SOLUTION I CAME UP WITH
            usrpss = str(self.username)+":"+str(self.password)
            encodedBytes = base64.b64encode(usrpss.encode("utf-8"))
            encodedStr = str(encodedBytes, "utf-8")
            p.setopt(
                pycurl.HTTPHEADER,
                [
                    "Host: " + self.host,
                    "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                    "Accept-Language: en-US,en;q=0.5",
                    "Accept-Encoding: gzip, deflate, br",
                    "Authorization: Basic " + encodedStr ,
                    "Connection: keep-alive",
                    "Content-Type: application/x-www-form-urlencoded",
                ],
            )
            p.setopt(p.URL, argv[1])
            p.setopt(
                pycurl.USERAGENT,
                "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:29.0) Gecko/20100101 Firefox/29.0",
            )

            p.setopt(pycurl.USERNAME, self.username)
            p.setopt(pycurl.USERPWD, urllib.parse.quote(str(self.password)))

            p.setopt(pycurl.SSL_VERIFYPEER, False)
            p.setopt(pycurl.SSL_VERIFYHOST, False)
            p.setopt(pycurl.COOKIEJAR, self.cFile)
            p.setopt(pycurl.COOKIEFILE, self.cFile)
            p.setopt(pycurl.FOLLOWLOCATION, True)
            p.setopt(pycurl.HEADERFUNCTION, self.ret.write)
            p.setopt(pycurl.WRITEFUNCTION, ret.write)

            if len(kwargs) > 0:
                p.setopt(pycurl.POST, True)
                p.setopt(pycurl.POSTFIELDS, kwargs)

            if(self.log):
                print("[INFO] Trying to connect...")
            try:
                p.perform()
                if(self.log):
                    print("[INFO] SUCCESS")
            ### IF YOU NEED TO CHECK THE HEADER, UNCOMMENT NEXTLINE
                    # print("[HEADER]\n"+self.ret.getvalue().decode("ISO-8859-1") + "\n\n" )
                return ret
            except pycurl.error as e:
                print(e)
            except:
                print(">> Error on Connection:")
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, fname, exc_tb.tb_lineno)
                return None
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            print(">> Generic Error:", sys.exc_info()[0])

    def signin(self, url, *arg):
        url = (
            "https://"
            + self.host
            + ":"
            + self.port
            + "/login/?login_only=1&user="
            + self.username
            + "&pass="
            + urllib.parse.quote(str(self.password))
        )
        if(self.log):
            print("[DEBUG] URL: " + url)
        replyAux = self.request(self, url)
        reply = replyAux.getvalue().decode("ISO-8859-1").split("{")
        if self.log:
            print("[DEBUG] Reply: {"+ reply[len(reply)-1])
        reply = json.loads("{"+ reply[len(reply)-1])
        self.cpsess = reply["security_token"]
        self.homepage = 'https://' + self.host+":"+self.port +reply['redirect']
        self.exPage = 'https://' + self.host+":"+self.port + self.cpsess+"/execute/"

    def __init__(self, host, port, username, password, log):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.log = log
        self.cFile = (
            os.getcwd() + "\\cookies\\cookie_" + str(random.randint(99999, 9999999)) + ".txt"
        )
        if self.log:
            # print("==================================================================")
            print("[DEBUG] Cookie_Path: " + os.getcwd() + "\\cookies\\cookie_" + str(random.randint(99999, 9999999)) + ".txt")
            # print("==================================================================")
        self.signin(self)

    def execute(self, api, module, function, *params):
        if api == 'api2':
            print("[WUPS] Not supported yet...")
            print("...ok, may not even in a closer future, only UAPI")
            return False
            ### This was written 21 february 2020
            ### I'M NOT MEAN, BUT I'D BEEN UNDER RESEARCH 
            #   AND DEVELOPMENT FOR A WEEK AND FOR FREE?
            #   ...
            #   I HAVE DEBTS TO PAY
        if api == 'uapi':
            uapi(self, module,function, params)
            return True
        print("[WARN] type " + api + ", not supported, only \"uapi\" for the moment")
        return False   



    def uapi(self, module, function, *params, **kwargs):
        if len(kwargs) > 0:
            parameters = urllib.parse.urlencode(kwargs)
        reply = self.request(self, self.exPage+module+"/"+function+"?"+parameters)
        return gzip.decompress(reply.getvalue()).decode("utf-8")



##SIMPLE EXAMPLE CODE
cpanel = cPanel("DOMAIN.COM", "2083", "USER", "PASSWORD", True)
print(cpanel.uapi("UserManager","list_users",flat="0"))
